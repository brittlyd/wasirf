clear; close all; clc;

%% Load in .dat file
% wkdir = 'C:\Users\britt\Dropbox (MREL)\WASIRF Data\07_6_21';
wkdir = 'G:\Dropbox (MREL)\WASIRF Data\07_6_21';
cd(wkdir);

N = 4; % number of data files (number of different sheets used)
M = 3; % number of wave gages

filename1 = "070621_Sine_T1_H10_0Sheet.dat";
filename2 = "070621_Sine_T1_H10_1Sheet.dat";
filename3 = "070621_Sine_T1_H10_2Sheet.dat";
filename4 = "070621_Sine_T1_H10_4Sheet.dat";
filenames = [filename1 filename2 filename3 filename4];

start_time_wavemaker = [5.9 4.5 7.3 7.1];
end_time_wavemaker = [64.3 77 80 94.5];

rampup_time = 10;
start_ramp_time_wavemaker = start_time_wavemaker + rampup_time;
end_ramp_time_wavemaker = end_time_wavemaker - rampup_time;

start_time_wavegage = [11.5 10.5 11.5 12.5; 15.8 13.5 17.5 17.1];
end_time_wavegage = start_time_wavegage + 70;
start_ramp_time_wavegage = start_time_wavegage + rampup_time;
end_ramp_time_wavegage = end_time_wavegage - rampup_time;

wg_data = struct();
for j = 1:N % loop through each sheet
    data = importdata(filenames(j));
    wg_data(1,j).time = data.data(:,1);
    wg_data(1,j).wavegage = data.data(:,3); % wavemaker
    wg_data(2,j).wavegage = data.data(:,2);
    wg_data(3,j).wavegage = data.data(:,4);
end

%% Wavemaker theory
H_wave = 0.1; % 10 cm
T_wave = 1; % wave period
A_wave = H_wave/2; % wave amplitude
h = 0.54; % water depth [m]

omega = 2*pi/T_wave; % wave frequency, rad/s
g = 9.81; % m/s^2
const = omega^2*h/g; % constant for dispersion.m code, dimensionless

kh = dispersion(const); % dimensionless
k = kh/h; % wave number, 1/m
lambda = 2*pi/k; % wavelength, m

%%% deep wave - >= 0.5
%%% shallow wave - <= 0.05
depth_regime = h/lambda; %%% nondimensional parameter to determine wave type

%%% apply linear wavemaker theory
S = H_wave*(sinh(kh)*cosh(kh)+kh)/(2*(sinh(kh)^2)); % piston stroke, cm
A_piston = S/2;

e = @(t,phi) A_piston.*sin(omega.*t+phi); % piston motion, cm
eta = @(t,phi) H_wave/2.*cos(omega.*t+phi); % wave elevation, cm %%%% why not kx - wt ???

c = sqrt((g/k).*tanh(kh)); % phase velocity, m/s
cg = (c/2)*(1+(2*kh)/(sinh(2*kh))); % group velocity, m/s


%% Find indices for cut data
%%% Find closest indices of important times using min function
%%% Is there a better way to do this ?? Maybe using find ??

ind_start = zeros(M,N);
ind_end = zeros(M,N);
ind_ramp_start = zeros(M,N);
ind_ramp_end = zeros(M,N);

for k = 1 %:M 1 = wavemaker
    for j = 1:N % sheets
        [~,ind_start(k,j)] = min(abs(start_time_wavemaker(j)-wg_data(1,j).time)); % index for piston start time
        [~,ind_end(k,j)] = min(abs(end_time_wavemaker(j)-wg_data(1,j).time)); % index for piston end time

        [~,ind_ramp_start(k,j)] = min(abs(start_ramp_time_wavemaker(k,j)-wg_data(1,j).time)); % index for ramp start time
        [~,ind_ramp_end(k,j)] = min(abs(end_ramp_time_wavemaker(k,j)-wg_data(1,j).time)); % index for ramp end time
    
        wg_data(k,j).time_cut = wg_data(1,j).time(ind_ramp_start(k,j):ind_ramp_end(k,j)) - wg_data(1,j).time(ind_ramp_start(k,j));
        wg_data(k,j).wavegage_cut = wg_data(k,j).wavegage(ind_ramp_start(k,j):ind_ramp_end(k,j)) - mean(wg_data(k,j).wavegage(1:ind_start(k,j)));
    
    end
end

%% Plot wavemaker data
sheets = [0, 1, 2, 4];
for k = 1 %:M
    f = figure;
    f.Position = [0,0,1300,1000];
    for j = 1:N
        ax(j) = subplot(N,1,j);
        plot(wg_data(k,j).time,wg_data(k,j).wavegage,'linewidth',1.5)
        hold on
        plot(start_time_wavemaker(k,j)*ones(size(wg_data(k,j).time)),wg_data(k,j).wavegage,'--','linewidth',1.5)
        plot(end_time_wavemaker(k,j)*ones(size(wg_data(k,j).time)),wg_data(k,j).wavegage,'--','linewidth',1.5)        
        plot(start_ramp_time_wavemaker(k,j)*ones(size(wg_data(k,j).time)),wg_data(k,j).wavegage,'--','linewidth',1.5)
        plot(end_ramp_time_wavemaker(k,j)*ones(size(wg_data(k,j).time)),wg_data(k,j).wavegage,'--','linewidth',1.5)
        Legend{j} = strcat(num2str(sheets(j)),' Sheets');
        ylabel(strcat(num2str(sheets(j)),' sheets [cm]'))

    end
    legend(Legend)
    xlabel('time [s]')
    title(ax(1),'Raw wavemaker data with cut times')
end

%%
xcorr_wavemaker = struct();
for k = 1 %:M
    f = figure;
    f.Position = [0,0,1300,1000];
    for j = 1:N
        e_theory = e(wg_data(k,j).time_cut,0)*100;
        fs = 1/(wg_data(k,j).time_cut(2)-wg_data(k,j).time_cut(1));
        
        [r,lags] = xcorr(wg_data(k,j).wavegage_cut,e_theory,'coeff');

        lag = lags(find(r==max(r),1,'first'));  %determine the lag with the maximum correlation coefficient
        t_offset = abs(lag)/fs;                %convert lag to time basis (fs = sample rate)
        r_max = r(r==max(r));             %store maximum correlation coefficient
        
        xcorr_wavemaker(k,j).fs = fs;
        xcorr_wavemaker(k,j).r = r;
        xcorr_wavemaker(k,j).lags = lags;
        xcorr_wavemaker(k,j).lag = lag;
        xcorr_wavemaker(k,j).t_offset = t_offset;
        xcorr_wavemaker(k,j).r_max = r_max;
        
        ax(j) = subplot(N,1,j);
        if lag > 0 
            plot(wg_data(k,j).time_cut(abs(lag):end) - t_offset,wg_data(k,j).wavegage_cut(abs(lag):end),'linewidth',1.5)
            hold on
            plot(wg_data(k,j).time_cut,e_theory,'linewidth',1)
        else 
            plot(wg_data(k,j).time_cut + t_offset,wg_data(k,j).wavegage_cut,'linewidth',1.5)
            hold on
            plot(wg_data(k,j).time_cut(abs(lag):end),e_theory(abs(lag):end),'linewidth',1)
        end
        grid minor
        xlim([0 30])
        legend('Data','Theory')
        ylabel(strcat(num2str(sheets(j)),' sheets [cm]'))
    end
    xlabel('time [s]')
    title(ax(1),'Wavemaker motion compared to theory')
end

%%
f = figure;
f.Position = [0,0,1500,500];
k = 1;
for j = 1:N
    
    fs = xcorr_wavemaker(k,j).fs;
    r = xcorr_wavemaker(k,j).r;
    lags = xcorr_wavemaker(k,j).lags;
    lag = xcorr_wavemaker(k,j).lag;
    t_offset = xcorr_wavemaker(k,j).t_offset;
    r_max = xcorr_wavemaker(k,j).r_max;
    
    
    if lag > 0 
        plot(wg_data(k,j).time_cut(abs(lag):end) - t_offset,wg_data(k,j).wavegage_cut(abs(lag):end),'linewidth',1.5)
        hold on
    else 
        plot(wg_data(k,j).time_cut + t_offset,wg_data(k,j).wavegage_cut,'linewidth',1.5)
        hold on
    end
    Legend{j} = strcat(num2str(sheets(j)),' Sheets');
end
plot(wg_data(k,j).time_cut,100*e(wg_data(k,j).time_cut,0),'linewidth',1)
grid minor

Legend{j+1} = 'Theory';
legend(Legend,'orientation','horizontal','location','N')

xlim([0 30])
xlabel('time [s]')
ylabel('Wavemaker motion [cm]')
title('Wavemaker motion for each sheet test with theory')


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Find indices for cut data - wavegage
%%% Find closest indices of important times using min function
%%% Is there a better way to do this ?? Maybe using find ??
for k = 2:M
    for j = 1:N % sheets
        [~,ind_start(k,j)] = min(abs(start_time_wavegage(k-1,j)-wg_data(1,j).time)); % index for piston start time
        [~,ind_end(k,j)] = min(abs(end_time_wavegage(k-1,j)-wg_data(1,j).time)); % index for piston end time

        [~,ind_ramp_start(k,j)] = min(abs(start_ramp_time_wavegage(k-1,j)-wg_data(1,j).time)); % index for ramp start time
        [~,ind_ramp_end(k,j)] = min(abs(end_ramp_time_wavegage(k-1,j)-wg_data(1,j).time)); % index for ramp end time
    
        wg_data(k,j).time_cut = wg_data(1,j).time(ind_ramp_start(k,j):ind_ramp_end(k,j)) - wg_data(1,j).time(ind_ramp_start(k,j));
        wg_data(k,j).wavegage_cut = wg_data(k,j).wavegage(ind_ramp_start(k,j):ind_ramp_end(k,j)) - mean(wg_data(k,j).wavegage(1:ind_start(k,j)));
    
    end
end

%% Plot second and third wave gage
for k = 2:M
    f = figure; % figure 4,5
    f.Position = [0,0,1300,1000];
    for j = 1:N
        ax(j) = subplot(N,1,j);
        plot(wg_data(1,j).time,wg_data(k,j).wavegage,'linewidth',1.5)
        hold on
        plot(start_time_wavegage(k-1,j)*ones(size(wg_data(1,j).time)),wg_data(k,j).wavegage,'--','linewidth',1.5)
        plot(end_time_wavegage(k-1,j)*ones(size(wg_data(1,j).time)),wg_data(k,j).wavegage,'--','linewidth',1.5)        
        plot(start_ramp_time_wavegage(k-1,j)*ones(size(wg_data(1,j).time)),wg_data(k,j).wavegage,'--','linewidth',1.5)
        plot(end_ramp_time_wavegage(k-1,j)*ones(size(wg_data(1,j).time)),wg_data(k,j).wavegage,'--','linewidth',1.5)
        Legend{j} = strcat(num2str(sheets(j)),' Sheets');
        ylabel(strcat(num2str(sheets(j)),' sheets [cm]'))

    end
    legend(Legend)
    xlabel('time [s]')
    title(ax(1),'Raw wavegage data with cut times')
end

%%
xcorr_wavegage = struct();
for k = 2:M
    f = figure;
    f.Position = [0,0,1300,1000];
    for j = 1:N
        eta_theory = eta(wg_data(k,j).time_cut,0)*100;
        fs = 1/(wg_data(k,j).time_cut(2)-wg_data(k,j).time_cut(1));
        
        [r,lags] = xcorr(wg_data(k,j).wavegage_cut,eta_theory,'coeff');

        lag = lags(find(r==max(r),1,'first'));  %determine the lag with the maximum correlation coefficient
        t_offset = abs(lag)/fs;                %convert lag to time basis (fs = sample rate)
        r_max = r(r==max(r));             %store maximum correlation coefficient
        
        xcorr_wavegage(k,j).fs = fs;
        xcorr_wavegage(k,j).r = r;
        xcorr_wavegage(k,j).lags = lags;
        xcorr_wavegage(k,j).lag = lag;
        xcorr_wavegage(k,j).t_offset = t_offset;
        xcorr_wavegage(k,j).r_max = r_max;
        
        ax(j) = subplot(N,1,j);
        if lag > 0 
            plot(wg_data(k,j).time_cut(abs(lag):end) - t_offset,wg_data(k,j).wavegage_cut(abs(lag):end),'linewidth',1.5)
            hold on
            plot(wg_data(k,j).time_cut,eta_theory,'linewidth',1)
        else 
            plot(wg_data(k,j).time_cut + t_offset,wg_data(k,j).wavegage_cut,'linewidth',1.5)
            hold on
            plot(wg_data(k,j).time_cut(abs(lag):end),eta_theory(abs(lag):end),'linewidth',1)
        end
        grid minor
        xlim([0 50])
        legend('Data','Theory')
        ylabel(strcat(num2str(sheets(j)),' sheets [cm]'))
    end
    xlabel('time [s]')
    title(ax(1),'Wavegage motion compared to theory')
end
 
%%
f = figure;
f.Position = [0,0,1500,500];
k = 2;
for j = 1:N
    
    fs = xcorr_wavegage(k,j).fs;
    r = xcorr_wavegage(k,j).r;
    lags = xcorr_wavegage(k,j).lags;
    lag = xcorr_wavegage(k,j).lag;
    t_offset = xcorr_wavegage(k,j).t_offset;
    r_max = xcorr_wavegage(k,j).r_max;
    
    
    if lag > 0 
        plot(wg_data(k,j).time_cut(abs(lag):end) - t_offset,wg_data(k,j).wavegage_cut(abs(lag):end),'linewidth',1.5)
        hold on
    else 
        plot(wg_data(k,j).time_cut + t_offset,wg_data(k,j).wavegage_cut,'linewidth',1.5)
        hold on
    end
    Legend{j} = strcat(num2str(sheets(j)),' Sheets');
end
plot(wg_data(k,j).time_cut,100*eta(wg_data(k,j).time_cut,0),'linewidth',1)
grid minor

Legend{j+1} = 'Theory';
legend(Legend,'orientation','horizontal','location','N')

xlim([0 50])
xlabel('time [s]')
ylabel('Wavegage motion [cm]')
title('2nd wavegage motion for each sheet test with theory')
