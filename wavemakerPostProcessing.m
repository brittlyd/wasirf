clear; close all; clc;

%% Load in .dat file
% wkdir = 'C:\Users\britt\Dropbox (MREL)\WASIRF Data\04_15_21'; % where data is stored
% wkdir = 'C:\Users\britt\Dropbox (MREL)\WASIRF Data\07_6_21';
wkdir = 'G:\Dropbox (MREL)\WASIRF Data\04_15_21';
cd(wkdir);

filename = '04152021_Sine_T1_A1.dat';
data = importdata(filename);

%% Define data parameters
rampup_time = 10; % how long it takes piston motion to reach full amplitude [s]
A_piston = 1; % amplitude of piston motion [cm]
T_piston = 1; % period of piston motion [s]
h = 0.7; % water depth [m]

if A_piston == 1 && T_piston == 1    
    start_time_piston_value = 8.5;
    end_time_piston_value = 50;
elseif A_piston == 2 && T_piston == 1    
    start_time_piston_value = 6;
    end_time_piston_value = 61;   
elseif A_piston == 3 && T_piston == 1    
    start_time_piston_value = 7.3;
    end_time_piston_value = 55;   
elseif A_piston == 2 && T_piston == 1.5    
    start_time_piston_value = 9;
    end_time_piston_value = 67; 
else 
    error('Piston amplitude or period is not documented in data')
end

%% Theory
omega = 2*pi/T_piston; % wave frequency, rad/s
g = 9.81; % m/s^2
const = omega^2*h/g; % constant for dispersion.m code, dimensionless

kh = dispersion(const); % dimensionless
k = kh/h; % wave number, 1/m
lambda = 2*pi/k; % wavelength, m

%%% deep wave - >= 0.5
%%% shallow wave - <= 0.05
depth_regime = h/lambda; %%% nondimensional parameter to determine wave type

%%% apply linear wavemaker theory
S = 2*A_piston; % piston stroke, cm
H = (2*S*(sinh(kh)^2))/(sinh(kh)*cosh(kh)+kh); % subsequent wave height, cm

e = @(t,phi) A_piston.*sin(omega.*t+phi); % piston motion, cm
eta = @(t,phi) H/2.*cos(omega.*t+phi); % wave elevation, cm %%%% why not kx - wt ???

c = sqrt((g/k).*tanh(kh)); % phase velocity, m/s
cg = (c/2)*(1+(2*kh)/(sinh(2*kh))); % group velocity, m/s


%% Extract raw data
t_raw = data.data(:,1); % time data, s
wg1_raw = data.data(:,3); % wavemaker data, cm
wg2_raw = data.data(:,2); % wave gage data, cm

%%% Plot raw wavemaker and wave gage data
f1 = figure(1);
f1.Position = [119.5,93.5,1143.5,528.5];
ax1 = subplot(2,1,1); % wavemaker
plot(t_raw,wg1_raw,'linewidth',1.5)
hold on
grid minor
xlabel('Time [s]')
ylabel('Wave Gage Measurement [cm]')
title('Raw wavemaker data')

ax2 = subplot(2,1,2); % wave gage
plot(t_raw,wg2_raw,'linewidth',1.5)
hold on
grid minor
xlabel('Time [s]')
ylabel('Wave Gage Measurement [cm]')
title('Raw wave data')

linkaxes([ax1 ax2],'x')

%% Denoised data - wavemaker
wg1_raw1 = wdenoise(wg1_raw,11, ... % de-noise wavemaker data with predetermined coeffs
    'Wavelet', 'coif1', ...
    'DenoisingMethod', 'Bayes', ...
    'ThresholdRule', 'Soft', ...
    'NoiseEstimate', 'LevelIndependent');

wg1_raw_denoised = wg1_raw1; % rename

%%% Plot raw and denoised wavemaker data
f2 = figure(2);
f2.Position = [119.5,93.5,1143.5,528.5];
ax1 = subplot(2,1,1);
plot(t_raw,wg1_raw,'linewidth',1.5)
hold on
plot(t_raw,wg1_raw_denoised,'linewidth',1.5)
grid minor
xlabel('Time [s]')
ylabel('Wave Gage Measurement [cm]')
title('Raw wavemaker data')
legend('Raw','De-noised')

ax2 = subplot(2,1,2); % just denoised data
plot(t_raw,wg1_raw_denoised,'linewidth',1.5)
hold on
grid minor
xlabel('Time [s]')
ylabel('Wave Gage Measurement [cm]')
title('De-noised data')

linkaxes([ax1 ax2],'x')

%% Denoised data - wave gage
wg2_raw1 = wdenoise(wg2_raw,11, ... % de-noise wavemaker data with predetermined coeffs
    'Wavelet', 'coif1', ...
    'DenoisingMethod', 'Bayes', ...
    'ThresholdRule', 'Soft', ...
    'NoiseEstimate', 'LevelIndependent');

wg2_raw_denoised = wg2_raw1; % rename

%%% Plot raw and denoised wavemaker data
f3 = figure(3);
f3.Position = [119.5,93.5,1143.5,528.5];
ax1 = subplot(2,1,1);
plot(t_raw,wg2_raw,'linewidth',1.5)
hold on
plot(t_raw,wg2_raw_denoised,'linewidth',1.5)
grid minor
xlabel('Time [s]')
ylabel('Wave Gage Measurement [cm]')
title('Raw wave gage data')
legend('Raw','De-noised')

ax2 = subplot(2,1,2); % just denoised data
plot(t_raw,wg2_raw_denoised,'linewidth',1.5)
hold on
grid minor
xlabel('Time [s]')
ylabel('Wave Gage Measurement [cm]')
title('De-noised data')

linkaxes([ax1 ax2],'x')

%% Cut data time series - wavemaker
%%% Define start time of piston, calculate when it reaches full amplitude
%%% using rampup_time
start_time_piston = start_time_piston_value * ones(size(t_raw)); %%%%%%%%%%%%%%
start_ramp_time_piston = start_time_piston + rampup_time;

%%% Define the time when the piston becomes stationary and calculate the
%%% last time the piston is moving at full amplitude
end_time_piston = end_time_piston_value * ones(size(t_raw)); %%%%%%%%%%%%%%%%%
end_ramp_time_piston = end_time_piston - rampup_time;

%%% Find closest indices of important times using min function
%%% Is there a better way to do this ?? Maybe using find ??
[~,ind_start] = min(abs(start_time_piston-t_raw)); % index for piston start time
[~,ind_end] = min(abs(end_time_piston-t_raw)); % index for piston end time

[~,ind_ramp_start] = min(abs(start_ramp_time_piston-t_raw)); % index for ramp start time
[~,ind_ramp_end] = min(abs(end_ramp_time_piston-t_raw)); % index for ramp end time

%% Cut data time series - wave gage
%%% Define first time the wave reaches wave gage and calculate when it
%%% reaches full amplitude using rampup_time
%%% TODO: Run new tests knowing the distance between wave gages and try to
%%% calculate this using wave speed
start_time_wave = 10.3 * ones(size(t_raw)); %%%%%%%%%%%%%%%%
start_ramp_time_wave = start_time_wave + rampup_time;

%%% Calculate the end time of the wave gage using the difference in start
%%% times between piston and wave gage and the end time of the piston
t_length = end_time_piston(1) - start_time_piston(1); % total length of time of piston motion
end_time_wave = start_time_wave + t_length;
end_ramp_time_wave = end_time_wave - rampup_time;



%% Plot cut times - wavemaker and wave gage
%%% cut time
t = t_raw(ind_ramp_start:ind_ramp_end) - t_raw(ind_ramp_start);

%%%%%%%%% this is where we detrend data using the mean of the raw data up
%%%%%%%%% to ind_ramp_start

%%% wavemaker cut data - raw and denoised
wg1 = wg1_raw(ind_ramp_start:ind_ramp_end) - mean(wg1_raw(1:ind_ramp_start));
wg1_denoised = wg1_raw_denoised(ind_ramp_start:ind_ramp_end) - mean(wg1_raw_denoised(1:ind_ramp_start));

%%% wave gage cut data - raw and denoised
wg2 = wg2_raw(ind_ramp_start:ind_ramp_end) - mean(wg2_raw(1:ind_ramp_start));
wg2_denoised = wg2_raw_denoised(ind_ramp_start:ind_ramp_end) - mean(wg2_raw_denoised(1:ind_ramp_start));

f4 = figure(4);
f4.Position = [119.5,93.5,1143.5,400];
ax1 = subplot(2,1,1);
grid minor
plot(t_raw,wg1_raw-mean(wg1_raw),'linewidth',1.5)
hold on
plot(t_raw,wg1_raw_denoised-mean(wg1_raw_denoised),'linewidth',1.5)
plot(start_time_piston,linspace(-A_piston-1,A_piston+1,length(start_time_piston)),'--','linewidth',1)
plot(end_time_piston,linspace(-A_piston-1,A_piston+1,length(start_time_piston)),'--','linewidth',1)
plot(end_ramp_time_piston,linspace(-A_piston-1,A_piston+1,length(start_time_piston)),'--','linewidth',1)
plot(start_ramp_time_piston,linspace(-A_piston-1,A_piston+1,length(start_time_piston)),'--','linewidth',1)
xlabel('Time [s]')
ylabel('Wave Gage Measurement [cm]')
title('Wavemaker Data')

ax2 = subplot(2,1,2);
grid minor
plot(t_raw,wg2_raw-mean(wg2_raw),'linewidth',1.5)
hold on

%%% wavemaker times
plot(t_raw,wg2_raw_denoised-mean(wg2_raw_denoised),'linewidth',1.5)
plot(start_time_piston,linspace(-A_piston-2,A_piston+2,length(start_time_piston)),'--','linewidth',1)
plot(end_time_piston,linspace(-A_piston-2,A_piston+2,length(start_time_piston)),'--','linewidth',1)
plot(end_ramp_time_piston,linspace(-A_piston-2,A_piston+2,length(start_time_piston)),'--','linewidth',1)
plot(start_ramp_time_piston,linspace(-A_piston-2,A_piston+2,length(start_time_piston)),'--','linewidth',1)

%%% wave times
plot(start_time_wave,linspace(-A_piston-2,A_piston+2,length(start_time_piston)),'--','linewidth',1)
plot(end_time_wave,linspace(-A_piston-2,A_piston+2,length(start_time_piston)),'--','linewidth',1)
plot(end_ramp_time_wave,linspace(-A_piston-2,A_piston+2,length(start_time_piston)),'--','linewidth',1)
plot(start_ramp_time_wave,linspace(-A_piston-2,A_piston+2,length(start_time_piston)),'--','linewidth',1)

xlabel('Time [s]')
ylabel('Wave Gage Measurement [cm]')
title('Wave gage data')

%% Plot cut data - wavemaker
f5 = figure(5);
f5.Position = [119.5,93.5,1143.5,528.5];
plot(t,wg1,'linewidth',1.5)
hold on
plot(t,wg1_denoised,'linewidth',1.5)
grid minor
xlabel('Time [s]')
ylabel('Wave Gage Measurement [cm]')
title('Cut wavemaker data')
legend('Raw','De-noised')

%% Plot cut data - wave gage
f6 = figure(6);
f6.Position = [119.5,93.5,1143.5,528.5];
plot(t,wg2,'linewidth',1.5)
hold on
plot(t,wg2_denoised,'linewidth',1.5)
grid minor
xlabel('Time [s]')
ylabel('Wave Gage Measurement [cm]')
title('Cut wave gage data')
legend('Raw','De-noised')

%% xcorr
% phi = 3.8; %%%%%%%%%%%%%%%%%%%%%% tuning parameter for theory
fs = 1/(t(2)-t(1));
e_theory = e(t,0);
eta_theory = eta(t,0);

% calculate cross-correlation between denoised data and theory

%%% wavemaker
% note: need to detrend by the mean to avoid having the DC offset
[r_piston,lags_piston] = xcorr(wg1_denoised,e_theory,'coeff');

lag_piston = lags_piston(find(r_piston==max(r_piston),1,'first'));  %determine the lag with the maximum correlation coefficient
t_offset_piston = lag_piston/fs;                %convert lag to time basis (fs = sample rate)
r_max_piston = r_piston(r_piston==max(r_piston));             %store maximum correlation coefficient

%%% wave gage
% note: need to detrend by the mean to avoid having the DC offset
[r_wave,lags_wave] = xcorr(wg2_denoised,eta_theory,'coeff');

lag_wave = lags_wave(find(r_wave==max(r_wave),1,'first'));  %determine the lag with the maximum correlation coefficient
t_offset_wave = lag_wave/fs;                %convert lag to time basis (fs = sample rate)
r_max_wave = r_wave(r_wave==max(r_wave));             %store maximum correlation coefficient

%% Plot xcorr results
%%% wavemaker
f7 = figure(7);
f7.Position = [119.5,93.5,1143.5,528.5];
if lag_piston > 0
    plot(t(lag_piston:end) - t_offset_piston, wg1_denoised(lag_piston:end),'linewidth',1.5)
    hold on
    plot(t,e_theory,'linewidth',1.5)
else 
    plot(t + t_offset_piston, wg1_denoised,'linewidth',1.5)
    hold on
    plot(t(abs(lag_piston):end),e_theory(abs(lag_piston):end),'linewidth',1.5)
end
grid minor
title('Correlated wavemaker data and theory')
xlabel('Time [s]')
ylabel('Wave Gage Measurement [cm]')
legend('Raw Data','Theory')

%%% wave gage
f8 = figure(8);
f8.Position = [119.5,93.5,1143.5,528.5];
plot(t(lag_wave:end) - t_offset_wave, wg2_denoised(lag_wave:end),'linewidth',1.5)
hold on
plot(t,eta_theory,'linewidth',1.5)
grid minor
title('Correlated wave gage data and theory')
xlabel('Time [s]')
ylabel('Wave Gage Measurement [cm]')
legend('Raw Data','Theory')

%% Error - wavemaker
L_tank = 12; % m
dist_tank_wg = 2*L_tank; % distance wave must travel to reach piston on reflection
t_tank_piston = dist_tank_wg/c; % time it takes for reflected waves to reach piston based on wave speed c
% t_g_tank_wg = dist_tank_wg/cg; % time it takes for reflected waves to reach piston based on group velocity cg

f9 = figure(9);
f9.Position = [119.5,93.5,1143.5,528.5];
    plot(t(lag_piston:end) - t_offset_piston, wg1_denoised(lag_piston:end),'linewidth',1.5)
    hold on
    plot(t(lag_piston:end) - t_offset_piston, wg1_denoised(lag_piston:end) - e_theory(1:end-lag_piston+1),'linewidth',1.5)
plot((t_tank_piston - start_ramp_time_piston(1))*ones(size(t)),wg1,'--','linewidth',1)
plot((t_tank_piston - start_time_piston(1))*ones(size(t)),wg1,'--','linewidth',1)
% plot(t_g_tank_wg*ones(size(t)),wg1,'--','linewidth',1)
grid minor
xlabel('Time [s]')
ylabel('Wave Gage Measurement [cm]')
title('Wavemaker Error')
legend('Raw','Error')

%% Error - wave gage

f10 = figure(10);
f10.Position = [119.5,93.5,1143.5,528.5];
plot(t(lag_wave:end) - t_offset_wave,wg2(lag_wave:end),'linewidth',1)
hold on
plot(t(lag_wave:end) - t_offset_wave,wg2_denoised(lag_wave:end) - eta_theory(1:end-lag_wave+1),'linewidth',1.5)
plot((t_tank_piston - start_ramp_time_piston(1))*ones(size(t)),wg1,'--','linewidth',1)
plot((t_tank_piston - start_time_piston(1))*ones(size(t)),wg1,'--','linewidth',1)
% plot(t_g_tank_wg*ones(size(t)),wg1,'--','linewidth',1)
grid minor
xlabel('Time [s]')
ylabel('Wave Gage Measurement [cm]')
title('Wave Gage Error')
legend('Raw','Error')

% %% Frequency content
% Fs = 1/(t(2)-t(1));
% L = length(t);
% n = 2^nextpow2(L);
% f = Fs*(0:(n/2))/n;
% wg1_fft = fft(wg1,n);
% P = abs(wg1_fft./n).^2;
% 
% f_main = 1/T_piston;
% 
% f9 = figure(9);
% f9.Position = [119.5,93.5,1143.5,528.5];
% plot(f,P(1:n/2+1),'linewidth',1.5)
% hold on
% plot(f_main*ones(size(P(1:n/2+1))),P(1:n/2+1),'r--')
% xlabel('Frequency [Hz]')
% ylabel('|P(\omega)|')
% xlim([0 (f_main+5)])




