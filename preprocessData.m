function [wgData,encoderData] = preprocessData(wavegageData,positionData,num_wg,t_start,t_end,t_noise,thresh)

t_cell = wavegageData.textdata(4:end);

date_format = 'HH:MM:SS.FFF';
t_raw = datenum(t_cell,date_format) * 24 * 60 * 60; % time in days converted to seconds
t_raw = t_raw - t_raw(1); % time vector in seconds

%% Extract encoder data
t_encoder_cell = positionData.textdata(3:end);
t_encoder = datenum(t_encoder_cell,date_format) * 24 * 60 * 60;
t_encoder = t_encoder - t_encoder(1);

count_conversion = 9102.222222222;
encoder_counts = positionData.data - mean(positionData.data);
encoder_position = encoder_counts / count_conversion / 100; % paddle position in m

%% Wavegage data
wg_raw = zeros(length(t_raw),num_wg);
wg_raw(:,2) = wavegageData.data(:,2)/100;
wg_raw(:,3) = wavegageData.data(:,3)/100;
wg_raw(:,1) = wavegageData.data(:,4)/100;

%% Clean up wavegage data
[~,t_ind_start] = min(abs(t_raw - t_start));
[~,t_ind_end] = min(abs(t_raw - t_end));
[~,t_ind_noise] = min(abs(t_raw - t_noise));

offset = mean(wg_raw(1:t_ind_noise,:));
offset_mat = repmat(offset,[length(t_raw) 1]);
wg_shifted = wg_raw - offset_mat;

t = t_raw(t_ind_start:t_ind_end) - t_raw(t_ind_start);
wg_spiky = wg_shifted(t_ind_start:t_ind_end,:);
[wg, ip] = rmoutliers(wg_spiky,'percentile',thresh); %func_despike_phasespace3d(wg_spiky, 0, 2);
t(ip) = [];


wgData = struct();
wgData.wg = wg;
wgData.t = t;
wgData.wg_raw = wg_raw;
wgData.wg_shifted = wg_shifted;
wgData.t_raw = t_raw;
wgData.wg_spiky = wg_spiky;

encoderData = struct();
encoderData.t = t_encoder;
encoderData.position = encoder_position;


end