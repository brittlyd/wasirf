clear; close all; clc;

user = 'Brittany';
%% Define wave and beach properties
H = [0.05 0.10]; % wave height (m)
T = [1 1.5 2]; % wave period (s)
h = [0.60 0.55 0.50]; % water depth (m)
beachType = 1; % type of beach, 0 == no beach, 1 == plywood, 2 == blue foam, 3 == chicken wire

beach_type_name = ["no_beach" "plywood" "blue_foam" "chicken_wire"];
save_filename = strcat('reflection_coefficient_data_',beach_type_name(beachType+1));

%% Define time properties
t_start = 60;
t_end = 100;
t_noise = 5;
dt = 0.0025;

%% Define wave gage properties
num_wg = 3;
thresh = [1 100];

dx_12 = 0.5; % distance between first and second wave gage, m
dx_23 = 0.5; % distance between second and third wave gage, m
dx_13 = dx_12 + dx_23; % distance between third and first wave gage, m

dl(1) = 0;
dl(2) = dx_12;
dl(3) = dx_13;


for ii = 1:length(H)
    for jj = 1:length(T)
        for kk = 1:length(h)
            %% Load data
            [wavegageData,positionData,main_dir,data_dir] = loadBeachData(H(ii),T(jj),h(kk),beachType,user);

            %% Wave data and paddle motion theory
            [waveData(ii,jj,kk)] = calculateWaveData(H(ii),T(jj),h(kk));

            %% Preprocess data
            [wgData(ii,jj,kk),encoderData(ii,jj,kk)] = preprocessData(wavegageData,positionData,num_wg,t_start,t_end,t_noise,thresh);
        
            %% FFT
            [frequencyData(ii,jj,kk)] = calculateFFT(wgData(ii,jj,kk),waveData(ii,jj,kk),dt,num_wg);

            %% Calculate reflection coefficient
            [reflectionCoeff(ii,jj,kk)] = calculateCoeffs(num_wg,frequencyData(ii,jj,kk),waveData(ii,jj,kk),dl);
        
        end
    end
end

save(save_filename,'reflectionCoeff','encoderData','waveData','wgData','frequencyData','num_wg','dl')

%% 

color_shape = ["ko" "k^"; "ro" "r^"; "bo", "b^"];

close all
f1 = figure(1);
f1.Position = [704,814,1045,351];
for ii = 1:length(H)
%     for jj = 1:length(T)
        for kk = 1:length(h)
            plot(waveData(ii,1,kk).lambda,reflectionCoeff(ii,1,kk).K_gs,color_shape(kk,ii),'linewidth',1.5)
            hold on
            plot(waveData(ii,2,kk).lambda,reflectionCoeff(ii,2,kk).K_gs,color_shape(kk,ii),'linewidth',2.5)
            plot(waveData(ii,3,kk).lambda,reflectionCoeff(ii,3,kk).K_gs,color_shape(kk,ii),'linewidth',3.5)
%             plot(waveData(ii,jj,kk).lambda,reflectionCoeff(ii,jj,kk).K_mf,'bo','linewidth',1.5)
%             plot(waveData(ii,jj,kk).lambda,reflectionCoeff(ii,jj,kk).K_isaacson,'ro','linewidth',1.5)
        end
%     end
end
grid on
xlabel('\lambda [m]')
ylabel('K')
% legend('Goda and Suzuki', 'Mansard and Funke', 'Isaacson')


% %% Plot raw data
% f1 = figure(1);
% f1.Position = [20,92.5,1230,482.5];
% 
% for j = 1:num_wg
%    ax(j) = subplot(num_wg,1,j);
%    plot(wgData.t_raw,wgData.wg_raw(:,j),'linewidth',1.5)
%    grid minor
% end
% xlabel('time')
% linkaxes(ax,'x')
% 
% figure(10)
% for j = 1:num_wg
%    plot(wgData.t_raw,wgData.wg_raw(:,j),'linewidth',1.5)
%    hold on
% end
% 
% %% Compare paddle data
% f2 = figure(2);
% f2.Position = [20,92.5,1230,482.5];
% plot(encoderData.t,encoderData.position,'linewidth',1.5)
% hold on
% plot(encoderData.t,waveData.piston_amplitude*sin(waveData.omega*encoderData.t),'linewidth',1.5)
% xlim([0 60])
% legend('encoder','theory')
% 
% %% Plot shifted, cut data
% f3 = figure(3);
% f3.Position = [20,92.5,1230,482.5];
% 
% for j = 1:num_wg
%    subplot(num_wg,1,j)
%    plot(wgData.t,wgData.wg(:,j),'linewidth',1.5)
%    grid minor
% end
% xlabel('time [s]')
% ylabel('\eta [m]')
% 
% f4 = figure(4);
% f4.Position = [20,92.5,1230,482.5];
% 
% for j = 1:num_wg
%    plot(wgData.t,wgData.wg(:,j),'linewidth',1.5)
%    hold on
%    grid minor
% end
% xlabel('time [s]')
% ylabel('\eta [m]')
% 
% 
% f5 = figure(5);
% for j = 1:num_wg
%    plot(frequencyData.f,abs(frequencyData.Y),'linewidth',1.5)
%    hold on
%    plot(frequencyData.f(frequencyData.f_ind),abs(frequencyData.Fn(j)),'o')
%    xline(1/waveData.T,'m--','linewidth',1.5)
%    grid minor
% end
% xlabel('frequency [Hz]')
% xlim([0 5])
% 
% for j = 1:num_wg
%    figure
%    plot(frequencyData.f,abs(frequencyData.Y(:,j)),'linewidth',1.5)
%    hold on
%    plot(frequencyData.f(frequencyData.f_ind(j)),abs(frequencyData.Fn(j)),'o')
%    xline(1/waveData.T,'m--','linewidth',1.5)
%    grid minor
%    xlabel('frequency [Hz]')
%    xlim([0 5])
% end
% 
% 
