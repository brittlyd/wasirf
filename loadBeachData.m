function [wavegageData,positionData,main_dir,data_dir] = loadBeachData(H_j,T_j,h_j,beachType_j,user)

if beachType_j == 0
    data_folder = 'Lydon Beach Experiments - No Beach';
elseif beachType_j == 1
        data_folder = 'Lydon Beach Experiments - Plywood';
elseif beachType_j == 2
        data_folder = 'Lydon Beach Experiments - Blue Foam';
elseif beachType_j == 3
        data_folder = 'Lydon Beach Experiments - Chicken Wire';
end

if T_j == 1 || T_j == 2
    position_filename = strcat('T_',num2str(T_j),'_H_',num2str(H_j*100,'%02.f'),'_h_',num2str(h_j*100),'_WASIRF_Position.dat');
    wavegage_filename = strcat('T_',num2str(T_j),'_H_',num2str(H_j*100,'%02.f'),'_h_',num2str(h_j*100),'_WASIRF_wavegauges.dat');
end

if T_j == 1.5
    position_filename = strcat('T_1_5_H_',num2str(H_j*100,'%02.f'),'_h_',num2str(h_j*100),'_WASIRF_Position.dat');
    wavegage_filename = strcat('T_1_5_H_',num2str(H_j*100,'%02.f'),'_h_',num2str(h_j*100),'_WASIRF_wavegauges.dat');
end

% main_dir = 'C:\Users\Brittany\Dropbox (MREL)\WASIRF Data\';
main_dir = strcat('C:\Users\',user,'\Dropbox (MREL)\WASIRF Data\');
data_dir = strcat(main_dir,data_folder);
cd(data_dir)
addpath(data_dir)
cd(data_dir)
wavegageData = importdata(wavegage_filename);
positionData = importdata(position_filename,'\t');

end