function [h] = hj(j,num,N,df)

r = 0:N-1;

H1_real = @(k,dx) (1./(2.*cos(-k.*dx - pi/2))).*cos(k.*dx+pi/2);
H1_imag = @(k,dx) (1./(2.*cos(-k.*dx - pi/2))).*sin(k.*dx+pi/2);
H2_real = (1./(2.*cos(-k.*dx - pi/2))).*cos(-pi/2);
H2_imag = (1./(2.*cos(-k.*dx - pi/2))).*sin(-pi/2);

h_vec = zeros(1,length(r));
for rr = 1:legnth(r)
    f = r(rr)*df; % calculate frequency
    omega = 2*pi*f; % convert to rad/s
    constant = omega^2*d/g; % calculate constant for dispersion.m
    kd = dispersion(constant);
    k = kd/d; % wave number
    if num == 1
        h_vec(rr) = (H1_real(k,dx) + 1i.*H1_imag(k,dx))*exp(1i*2*pi*r(rr)*j/N);
    elseif num == 2
        h_vec(rr) = (H2_real(k,dx) + 1i.*H2_imag(k,dx))*exp(1i*2*pi*r(rr)*j/N);
    end
end

h = sum(h_vec);



end