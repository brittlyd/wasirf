function [frequencyData] = calculateFFT(wavegageData,waveData,dt,num_wg)
wg = wavegageData.wg;
t = wavegageData.t;

Fs = 1/dt;
L = length(t);
n = L; %2^nextpow2(L);

f = Fs*(0:(n/2))/n;
% Y = zeros(2*length(f)-1,num_wg);

% [~,f_ind] = min(abs(f - 1/waveData.T));
Fn = zeros(1,num_wg);

for j = 1:num_wg
    Y(:,j) = fft(wg(:,j),n)/(n/2);
    [~,f_ind(j)] = max(abs(Y(:,j)));
    Fn(j) = Y(f_ind(j),j);
end

frequencyData = struct();
frequencyData.Y = Y(1:(n/2+1),:);
frequencyData.Fn = Fn;
frequencyData.f = f;
frequencyData.f_ind = f_ind;

end