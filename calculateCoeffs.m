function [reflectionCoeff] = calculateCoeffs(num_wg,frequencyData,waveData,dl)
reflectionCoeff = struct();
%% Define variables in Isaacson
for j = 1:num_wg
   A(j) = abs(frequencyData.Fn(j));
   phase(j) = -angle(frequencyData.Fn(j)); %-atan(imag(Fn(j))/real(Fn(j)));
   delta(j) = phase(j) - phase(1);
   Delta(j) = waveData.k*dl(j);
end

reflectionCoeff.A = A;
reflectionCoeff.phase = phase;
reflectionCoeff.delta = delta;
reflectionCoeff.Delta = Delta;

%% Goda and Suzuki

A1_gs = A(1);
A2_gs = A(2);
Delta_gs = Delta(2);
delta_gs = delta(2);

ai_gs = (1/(2*abs(sin(Delta_gs))))*...
    sqrt(A1_gs^2 + A2_gs^2 - 2*A1_gs*A2_gs*cos(Delta_gs + delta_gs));

ar_gs = (1/(2*abs(sin(Delta_gs))))*...
    sqrt(A1_gs^2 + A2_gs^2 - 2*A1_gs*A2_gs*cos(Delta_gs - delta_gs));

K_gs = ar_gs/ai_gs;

reflectionCoeff.K_gs = K_gs;
reflectionCoeff.ai_gs = ai_gs;
reflectionCoeff.ar_gs = ar_gs;
%% Mansard and Funke
s1 = sum(exp(1i*2*Delta));
s2 = sum(exp(-1i*2*Delta));
s3 = sum(A.*exp(1i*(delta + Delta)));
s4 = sum(A.*exp(1i*(delta - Delta)));
s5 = s1*s2 - 9;

Xi = (s2*s3 - 3*s4)/s5;
Xr = (s1*s4 - 3*s3)/s5;

ai_mf = abs(Xi);
ar_mf = abs(Xr);
    
K_mf = ar_mf/ai_mf;

reflectionCoeff.K_mf = K_mf;
reflectionCoeff.ai_mf = ai_mf;
reflectionCoeff.ar_mf = ar_mf;

%% Isaacson

Lambda_num = A(1)^2*sin(2*(Delta(3)-Delta(2))) - A(2)^2*sin(2*Delta(3)) + ...
                A(3)^2*sin(2*Delta(2));
Lambda_denom = sin(2*(Delta(3) - Delta(2))) + sin(2*Delta(2)) - sin(2*Delta(3));
Lambda = Lambda_num/Lambda_denom;
Gamma = 0.5*sqrt(((A(1)^2 + A(3)^2 - 2*Lambda)/cos(Delta(3)))^2 + ...
                ((A(1)^2 - A(3)^2)/sin(Delta(3)))^2);
            
ai_isaacson = 0.5*(sqrt(Lambda + Gamma) + sqrt(Lambda - Gamma));
ar_isaacson = 0.5*(sqrt(Lambda + Gamma) - sqrt(Lambda - Gamma));
K_isaacson = ar_isaacson/ai_isaacson;

reflectionCoeff.K_isaacson = K_isaacson;
reflectionCoeff.ai_isaacson = ai_isaacson;
reflectionCoeff.ar_isaacson = ar_isaacson;

end