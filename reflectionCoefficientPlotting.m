clear; close all; clc;

user = 'Brittany';

H = [0.05 0.10]; % wave height (m)
T = [1 1.5 2]; % wave period (s)
h = [0.60 0.55 0.50]; % water depth (m)

beachTypeVec = 1:3; % type of beach, 0 == no beach, 1 == plywood, 2 == blue foam, 3 == chicken wire
% f1 = figure(1);
% f1.Position = [704,814,1045,351];
for bb = 1:length(beachTypeVec)
    beachType = beachTypeVec(bb);

    beach_type_filename = ["plywood" "blue_foam" "chicken_wire"];
    beach_type = ["Plywood" "Blue Foam" "Chicken Wire"];
    
    save_filename = strcat('reflection_coefficient_data_',beach_type_filename(beachType));
    data_folder = strcat("Lydon Beach Experiments - ",beach_type(beachType));
    
    main_dir = strcat('C:\Users\',user,'\Dropbox (MREL)\WASIRF Data\');
    data_dir = strcat(main_dir,data_folder);
    
    cd(data_dir)
    load(save_filename)
    
    colortype = 'brkgmcy';
    markertype = '.ox+*sdv^<>ph';
    linewidthtype = [0.5 1.5 3];
    
%     %%% Question 1: Do all three methods work?
%     f = figure;
%     f.Position = [704,814,1045,351];
%     count = 1;
%     for ii = 1:length(H)
%         for jj = 1:length(T)
%             for kk = 1:length(h)
%                 plot(waveData(ii,jj,kk).lambda,reflectionCoeff(ii,jj,kk).K_gs,strcat(colortype(count),markertype(2)),'linewidth',linewidthtype(2))
%                 hold on
%                 plot(waveData(ii,jj,kk).lambda,reflectionCoeff(ii,jj,kk).K_mf,strcat(colortype(count),markertype(6)),'linewidth',linewidthtype(2))
%                 plot(waveData(ii,jj,kk).lambda,reflectionCoeff(ii,jj,kk).K_isaacson,strcat(colortype(count),markertype(4)),'linewidth',linewidthtype(2))
%                 count = count + 1;
%     
%                 if count > 7
%                     count = 1;
%                 end
%             end
%         end
%     end
%     grid on
%     xlabel('\lambda [m]')
%     ylabel('K')
%     legend('Goda and Suzuki', 'Mansard and Funke', 'Isaacson','location','eastoutside')
%     title(beach_type(beachType))
% 
% 
%     count = 1;
%     for ii = 1:length(H)
%         f = figure;
%         f.Position = [704,814,1045,351];
% 
%         for jj = 1:length(T)
%             for kk = 1:length(h)
%                 plot(waveData(ii,jj,kk).lambda,reflectionCoeff(ii,jj,kk).ai_gs*100,strcat(colortype(count),markertype(2)),'linewidth',linewidthtype(2))
%                 hold on
%                 plot(waveData(ii,jj,kk).lambda,reflectionCoeff(ii,jj,kk).ai_mf*100,strcat(colortype(count),markertype(6)),'linewidth',linewidthtype(2))
%                 plot(waveData(ii,jj,kk).lambda,reflectionCoeff(ii,jj,kk).ai_isaacson*100,strcat(colortype(count),markertype(4)),'linewidth',linewidthtype(2))
%                 yline(100*waveData(ii,jj,kk).H/2,'linewidth',1.5)
%                 count = count + 1;
%     
%                 if count > 7
%                     count = 1;
%                 end
%             end
%         end
%         grid on
%         xlabel('\lambda [m]')
%         ylabel('A_i [cm]')
%         ylim([0 100*waveData(ii,jj,kk).H])
%         legend('Goda and Suzuki', 'Mansard and Funke', 'Isaacson','location','eastoutside')
%         title(strcat(beach_type(beachType),', A = ',num2str(100*waveData(ii,jj,kk).H/2),'cm'))
%     end


    %%% Question 2: Dependence on wavelength, wave height, water depth
    f = figure;
    f.Position = [704,814,1045,351];
    for ii = 1:length(H)
        for jj = 1:length(T)
            for kk = 1:length(h)
                plot(waveData(ii,1,kk).lambda,reflectionCoeff(ii,1,kk).K_mf,strcat(colortype(ii),markertype(kk)),'linewidth',linewidthtype(2))
                hold on
                plot(waveData(ii,2,kk).lambda,reflectionCoeff(ii,2,kk).K_mf,strcat(colortype(ii),markertype(kk)),'linewidth',linewidthtype(2))
                plot(waveData(ii,3,kk).lambda,reflectionCoeff(ii,3,kk).K_mf,strcat(colortype(ii),markertype(kk)),'linewidth',linewidthtype(2))
 
            end
        end
    end
    grid on
    xlabel('\lambda [m]')
    ylabel('K')
%     legend('Goda and Suzuki', 'Mansard and Funke', 'Isaacson','location','eastoutside')
    title(strcat(beach_type(beachType),' - Mansard and Funke'))

    %%% Question 3: Dependence on beach type
%     for ii = 1:length(H)
%         for jj = 1:length(T)
%             for kk = 1:length(h)
%                 plot(waveData(ii,1,kk).lambda,reflectionCoeff(ii,1,kk).K_mf,strcat(colortype(bb),markertype(ii)),'linewidth',linewidthtype(2))
%                 hold on
%                 plot(waveData(ii,2,kk).lambda,reflectionCoeff(ii,2,kk).K_mf,strcat(colortype(bb),markertype(ii)),'linewidth',linewidthtype(2))
%                 plot(waveData(ii,3,kk).lambda,reflectionCoeff(ii,3,kk).K_mf,strcat(colortype(bb),markertype(ii)),'linewidth',linewidthtype(2))
%  
%             end
%         end
%     end
%     grid on
%     xlabel('\lambda [m]')
%     ylabel('K')
%     title('Mansard and Funke')
%     ylim([0 1])

end
