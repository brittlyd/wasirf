function [waveData] = calculateWaveData(H,T,h)

a = H/2; % wave amplitude (m)

g = 9.81; % m/s^2
omega = 2*pi/T; % wave frequency
const = omega^2*h/g; % parameter for dispersion.m

kh = dispersion(const); % output from dispersion.m
k = kh/h; % wave number (m)
lambda = 2*pi/k; % wavelength (1/m)

HS_piston = @(kh) 2*(cosh(2*kh) - 1)./(sinh(2*kh)+2*kh); % H/S function for piston wavemaker
HS = HS_piston(kh);
S = H/HS;
piston_amplitude = S/2;

waveData = struct();
waveData.H = H;
waveData.T = T;
waveData.h = h;
waveData.a = a;
waveData.omega = omega;
waveData.k = k;
waveData.lambda = lambda;
waveData.piston_amplitude = piston_amplitude;



end