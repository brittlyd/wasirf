clear; close all; clc;

%% Wave parameters
g = 9.81; % gravity

T = 2; % wave period
H = 0.1; % wave height
a = H/2; % wave amplitude
h = 0.55; % water depth

omega = 2*pi/T; % wave frequency
const = omega^2*h/g; % parameter for dispersion.m

kh = dispersion(const); % output from dispersion.m
k = kh/h; % wave number
lambda = 2*pi/k % wavelength

u_max = a*omega*cosh(2*kh)/sinh(kh); % max horizontal velocity
w_max = a*omega*sinh(2*kh)/sinh(kh); % max vertical velocity

c = sqrt(g*tanh(kh)/k); % wave speed (phase velocity)
cg = (c/2)*(1+(2*kh)/sinh(2*kh));

regime_parameter = h/lambda;

wavemaker_type = 'piston'; % type of wavemaker paddle - piston or flap
save_bool = false;


%% Linear wavemaker theory
HS_piston = @(kh) 2*(cosh(2*kh) - 1)./(sinh(2*kh)+2*kh); % H/S function for piston wavemaker
HS_flap = @(kh) (4*(sinh(kh)./kh)).*(((kh.*sinh(kh)) - cosh(kh) + 1)./(sinh(2.*kh)+2.*kh)); % H/s function for flap wavemaker

if(strcmp(wavemaker_type,'piston'))
    HS = HS_piston(kh);
    S = H/HS
end

if(strcmp(wavemaker_type,'flap'))
    HS = HS_flap(kh);
    S = H/HS
end

amp = S/2 % what you put into the computer to generate this wave

%% Plotting parameters
kh_plot = linspace(0,10,100);
t = linspace(0,5*T,1000);
wavemaker_position = (S/2).*cos(omega.*t);
wavemaker_velocity = -(S/2).*omega.*sin(omega.*t);

%% Plot wavemaker regime
% f1 = figure(1);
% f1.Name = 'wavemaker_regime';
% 
% plot(kh_plot,HS_piston(kh_plot),'k','linewidth',1.5)
% hold on
% plot(kh_plot,HS_flap(kh_plot),'b','linewidth',1.5)
% plot(kh,HS,'ro','linewidth',1.5)
% xlabel('kh')
% ylabel('H/S')
% legend('piston','flap','location','SE')
% 
% %% Plot wavemaker time series
% f2 = figure(2);
% f2.Name = 'wavemaker_time_series';
% % f2.Position = [405,803,1985,407];
% 
% yyaxis left
% plot(t,wavemaker_position,'linewidth',1.5)
% ylabel('Position [m]')
% 
% yyaxis right
% plot(t,wavemaker_velocity,'linewidth',1.5)
% ylabel('Velocity [m/s]')
% 
% title('Wavemaker Kinematics')
% xlabel('Time [s]')
% xlim([0 t(end)])