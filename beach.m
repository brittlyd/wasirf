clear; close all; clc;

%% Wave parameters
H = 0.1; % wave height, m
A = H/2; % wave amplitude, m
T = 1; % wave period, s
omega = 2*pi/T; % wave frequency, rad/s
h = 0.65; % water depth, m

%% Dispersion
g = 9.81; % gravity
const = omega^2*h/g; % constant for dispersion.m, unitless
kh = dispersion(const);
k = kh/h; % wavenumber, 1/m
lambda = 2*pi/h; % wavelength, m

%% Beach
beach_slope = 5; % 1:x slope
beach_length = h * beach_slope;
wave_steepness = H/lambda; % k*a;
