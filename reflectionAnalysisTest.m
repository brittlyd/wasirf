clear; close all; clc;

K = 0.2;
ai = 0.05;
ar = K*ai;
beta = pi/3;

T = 2;
omega = 2*pi/T;

g = 9.81;
h = 0.5;
const = omega^2*h/g;
kh = dispersion(const);
k = kh/h;
lambda = 2*pi/k;

eta_i = @(x,t) ai * cos(k*x - omega*t);
eta_r = @(x,t) ar * cos(-k*x - omega*t + beta);

xn = [1:0.5:2];
tvec = linspace(0,10*T,1000);
dt = tvec(2) - tvec(1);

%% wavegage measurement
num_wg = length(xn);
eta_wg = zeros(length(tvec),num_wg);

figure(1)
for j = 1:num_wg
    eta_wg(:,j) = eta_i(xn(j),tvec) + eta_r(xn(j),tvec);

    plot(tvec,eta_wg(:,j),'linewidth',1.5)
    hold on
end
xlabel('time')
ylabel('\eta')

%% Lab perspective
xvec = linspace(0,3*lambda,100);

% figure(2)
% for tt = 1:length(tvec)
%    plot(xvec,eta_i(xvec,tvec(tt)),'linewidth',1.5)
%    hold on
%    plot(xvec,eta_r(xvec,tvec(tt)),'linewidth',1.5)
%    plot(xvec,eta_i(xvec,tvec(tt)) + eta_r(xvec,tvec(tt)),'linewidth',1.5)
% 
%    xline(xn(1),'linewidth',1.5)
%    xline(xn(2),'linewidth',1.5)
%    xline(xn(3),'linewidth',1.5)
%    
%    legend('\eta_i','\eta_r','\eta')
%    xlabel('x')
%    ylabel('Free surface elevation')
%    grid minor
%    xlim([0 3*lambda])
%    ylim([-1 1]*0.15)
%    
%    pause(tvec(2) - tvec(1))
%    clf
% end

%%
% %% break up time series for fft
% t_break = [2 4 6 8];
% t_ind_start = ones(size(t_break));
% t_ind_end = zeros(size(t_break));
% t_struct = struct();
% eta_struct = struct();
% figure(3)
% for j = 1:length(t_break)
%     [~,ind] = min(abs(tvec - t_break(j)));
%     t_ind_end(j) = ind;
%     if j < length(t_break)
%         t_ind_start(j+1) = ind;
%     end
%     t_struct(j).t = tvec(t_ind_start(j):t_ind_end(j));
%     for m = 1:num_wg
%         eta_struct(m,j).eta = eta_wg(t_ind_start(j):t_ind_end(j),m);
%     end
% end
% 
% for m = 1:num_wg
%     figure
%     for j = 1:length(t_break)
%         plot(t_struct(j).t,eta_struct(m,j).eta,'linewidth',1.5)
%         hold on
%     end
% end


%%
Fs = 1/dt;

L = length(tvec);
n = L; %2^nextpow2(L);

f = Fs*(0:(n/2))/n;
Y = zeros(2*length(f)-2,num_wg);

[~,f_ind] = min(abs(f - 1/T));
Fn = zeros(1,num_wg);

for j = 1:num_wg
    Y(:,j) = fft(eta_wg(:,j),n)/n;
    Fn(j) = Y(f_ind,j);
end

f5 = figure(5);
f5.Position = [20,92.5,1230,482.5];

for j = 1:num_wg
   plot(f,real(Y(1:(n/2+1),j)),'linewidth',1.5)
   hold on
   plot(f(f_ind),real(Fn(j)),'o')
   hold on
   grid minor
end
xlabel('time')
xlim([0 10])

for j = 1:num_wg
   figure
   plot(f,real(Y(1:(n/2+1),j)),'linewidth',1.5)
   hold on
   plot(f(f_ind),real(Fn(j)),'o')
   hold on
   grid minor
   xlabel('frequency')
   xlim([0 10])
end

% Y_struct = struct(); %zeros(2*length(f)-2,num_wg);
% 
% Fn = zeros(length(t_break),num_wg);
% 
% for j = 1:num_wg
%     for k = 1:length(t_break)
%         L = length(t_struct(j).t);
%         n = 2^nextpow2(L);
% 
%         f = Fs*(0:(n/2))/n;
%         [~,f_ind] = min(abs(f - 1/T));
% 
%         Y = fft(eta_struct(j).eta,n);
%         Y_struct(k,j).L = L;
%         Y_struct(k,j).n = n;
%         Y_struct(k,j).Y = Y;
%         Y_struct(k,j).f = f;
%         Y_struct(k,j).f_ind = f_ind;
%         Y_struct(k,j).Fn = Y(f_ind);
%         Fn(k,j) = Y(f_ind);
%     end
% end
% 
% 
% for j = 1:num_wg
%     figure
%     for k = 1:length(t_break)
%         plot(Y_struct(k,j).f,Y_struct(k,j).Y(1:(Y_struct(k,j).L)/2+1),'linewidth',1.5)
%         hold on
%         plot(Y_struct(k,j).f(Y_struct(k,j).f_ind),Fn(k,j),'o','linewidth',1.5)
%     end
%     xlabel('frequency')
%     ylabel('Fn')
%     title(strcat('Wave gage #',num2str(j)));
% end
% 
% 
%



dl(1) = 0;
dl(2) = xn(2) - xn(1);
dl(3) = xn(3) - xn(1);


for j = 1:num_wg
   A(j) = 2*abs(Fn(j));
   phase(j) = angle(Fn(j)); %-atan(imag(Fn(j))/real(Fn(j)));
   delta(j) = phase(j) - phase(1);
   Delta(j) = k*dl(j);
end

%% Goda and Suzuki

A1_gs = A(1);
A2_gs = A(2);
Delta_gs = Delta(2);
delta_gs = delta(2);

ai_gs = (1/(2*abs(sin(Delta_gs))))*...
    sqrt(A1_gs^2 + A2_gs^2 - 2*A1_gs*A2_gs*cos(Delta_gs + delta_gs));

ar_gs = (1/(2*abs(sin(Delta_gs))))*...
    sqrt(A1_gs^2 + A2_gs^2 - 2*A1_gs*A2_gs*cos(Delta_gs - delta_gs));

K_gs = ar_gs/ai_gs;


% %% Mansard and Funke
s1 = sum(exp(1i*2*Delta));
s2 = sum(exp(-1i*2*Delta));
s3 = sum(A.*exp(1i*(delta + Delta)));
s4 = sum(A.*exp(1i*(delta - Delta)));
s5 = s1*s2 - 9;

Xi = (s2*s3 - 3*s4)/s5;
Xr = (s1*s4 - 3*s3)/s5;

ai_mf = abs(Xi);
ar_mf = abs(Xr);

K_mf = ar_mf/ai_mf;
 
% %% Isaacson

Lambda_num = A(1)^2*sin(2*(Delta(3)-Delta(2))) - A(2)^2*sin(2*Delta(3)) + ...
                A(3)^2*sin(2*Delta(2));
Lambda_denom = sin(2*(Delta(3) - Delta(2))) + sin(2*Delta(2)) - sin(2*Delta(3));
Lambda = Lambda_num/Lambda_denom;
Gamma = 0.5*sqrt(((A(1)^2 + A(3)^2 - 2*Lambda)/cos(Delta(3)))^2 + ...
                ((A(1)^2 - A(3)^2)/sin(Delta(3)))^2);
            
ai_isaacson = 0.5*(sqrt(Lambda + Gamma) + sqrt(Lambda - Gamma));
ar_isaacson = 0.5*(sqrt(Lambda + Gamma) - sqrt(Lambda - Gamma));
K_isaacson = ar_isaacson/ai_isaacson;
